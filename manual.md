# Low Carbon Plugin User Manual

## Command Descriptions
- Show Emissions - This ccommand is used to analyse for network transfers in the views.py file and to gather their sizes and will then make an emissions estimate on the network transfers.
- Add Profiling - This command is used to add the silk profiler to the Django project in order to do our profiling
- Add Profiling Results - This command is used to analyse for each view in the views.py file and allows the user to add results from the profiler in order to make an emissions cost for the profiled code of that view
- Hide emissions - This command is used to hide and reset the emissions so the code can be analysed again

## Usage
### Optional
The Add profiling command can be used optionally to try and install the silk profiler used for profiling. In order to do this you need to open your projects settings.py file and start the command. It will then ask for the path to your MAIN urls file this is usally in the same directory as your settings.py file. You should then be able to access the profiler results by starting your django project and navigating to /silk at the end of your main project url. Full docs for the silk profiler can be found [here](https://silk.readthedocs.io/en/latest/) in case this command fails or any other issues persist.

### Main Instructions

**Step 1**

Open the views.py file you want to analyse and run the show emissions command. This will ask for your projects urls.py file path, this should be the one that links to all the views in your views.py file. Enter this path and then it will show all lines where data sizes are requested for network transfers. Enter these and you will get your network transfers emissions estimate.

**Step 2**

With the views.py file still open use the Add Profiling Results command to add the results from the profiler. It will again ask for the path to the urls.py file, this should be the same one used in step 1. It will then highlight where each view starts and ask for the profiler time in seconds. Enter this for each view and you will get the emissions cost for each views non-network transfer code. 

**Step 3**

The hide emissions command can the be used to effectively reset the pluging so you may analyse the code again after it has been changed.


### Additional information
- After the show emissions command completes you should see different coloured lines. Red means highest emissions, yellow means average and green means lowest within your code. There will always be an even split of these lines.

- In order to see the exact emissions of each network transfer you can hover over the lines they occur and a box will appear showing the emissions. The same can be done to see the profiling cost of each view by hovering over def() wheere the view is defined.

- A breakdown of the difference in totals between network transfers and profiled code can be viewed by hovering over the number in the taslbar which will bring up a box with more detailed information


