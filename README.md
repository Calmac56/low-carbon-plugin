# Low Carbon Plugin

  

## Name

Low carbon computing: An IDE plugin for analysis of the energy/emissions cost of coding decisions

  
  

## Description

This project is a plugin for Visual Studio Code that analyses the emissions cost of Django web applications using their views.py and urls.py files. It contains 4 commands that can be used inside of Visual Studio Command panel. Full details on usage are described in the manual.md file.

  
  

## Requirements

 - Visual Studio Code 
 - Python 3.7 or later
 - A Django Project to use
 


 ## Installations Instructions

 ### **Building from source**

 **Step 1**
    
  Clone the project using git clone as below:

    git clone https://gitlab.com/Calmac56/low-carbon-plugin.git


 **Step 2**

Open the carbon-emissions-of-networked-applications folder in Visual Studio Code. Navigate to the src folder inside the carbon-emissions-of-networked-applications folder. Open the extension.ts file in Visual Studio Code.

**Step 3**

Install the required npm packages. Open a terminal inside Visual Studio Code and type the following commands.

    cd carbon-emissions-of-networked-applications
    npm install


### **Installing directly to your visual studio Code Environment**

Open Visual Studio code from the carbon-emissions-of-networked-applications folder. Then open a terminal in Visual Studio Code type the following commands

    code --install-extension carbon-emissions-of-networked-applications-0.0.1.vsix

This will install the plugin directly into visual studio code. It can than be managed from the standard extension manager.




## Authors and acknowledgment

This was a project authored by Calum MacPhee and supervised by Wim Vanderbauwhede as part of Glasgow University Level 4 dissertation projects.

  

## License

This project is open source and licensed under GNU General Public License version 3.

