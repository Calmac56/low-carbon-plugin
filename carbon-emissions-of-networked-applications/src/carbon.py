import sys
import ast
from django.conf import settings

settings.configure()

#Creates the nodevisitor class used to visit each function definition
class FuncLister(ast.NodeVisitor):
    fullDict = {}
    callList = []
    thelist = []
    itemstoremove = []
    networklineslist = []
    nodeDict = {}
    funcList =  []
    i = 0
    urlsList = []
    
    #initialises the class and sets path to urls.py file
    def __init__(self,urlDoc):
        self.urlDoc = urlDoc

    #Function to check that every view analysed also appears in the urls file (So non-used views or other functions are ignored)
    def checkUrls(self):
        urlFile = open(self.urlDoc, 'r')
        for line in urlFile:
            if 'path' in line:
                line = line.strip("'").strip('\t').strip('\n')
                splitline = line.split(',')
                if len(splitline) >= 2:
                    splitline[1].strip(' ').strip("'")
                    splitline2 = splitline[1].split('.')
                    if(len(splitline2)>0):
                        self.urlsList.append(splitline2[1])

    #Function that goes through all child nodes of the function definitions in the AST (recursive)                 
    def iterChildNodes(self, node):
        #Checks whether the child node is a function definition
        if (isinstance(node, ast.FunctionDef)):
            self.fullDict[node] = self.callList
            self.callList = []
            self.nodeDict ={}
            if node.name in self.urlsList:
                self.networklineslist.append(node.lineno)
                self.funcList.append(node.lineno)
        
        for node in ast.iter_child_nodes(node):
            if isinstance(node, ast.If) or isinstance(node, ast.Try) or isinstance(node, ast.For) or isinstance(node, ast.While):
                self.thelist.append(node)
                self.iterChildNodes(node)
            
            #Checks whether the function is a return statement and if it contains render to add it to network tramsfer list
            elif isinstance(node, ast.Return) and 'render' in ast.dump(node):

                for i in range(len(self.thelist)):
                    if self.thelist[i].end_lineno <= node.lineno:
                        self.itemstoremove.append(self.thelist[i])

                self.thelist = list(set(self.thelist) - set(self.itemstoremove))
                self.nodeDict[node] = self.thelist
                self.callList.append(self.nodeDict)
                self.networklineslist.append(node.lineno)
                

                self.thelist = []
                
                return node
            
            #Checks whether the function is a return statement and if it contains HttpResponse to add it to network transfer list
            elif isinstance(node, ast.Return) and 'HttpResponse' in ast.dump(node):

                for i in range(len(self.thelist)):
                    if self.thelist[i].end_lineno <= node.lineno:
                        self.itemstoremove.append(self.thelist[i])

                self.thelist = list(set(self.thelist) - set(self.itemstoremove))
                self.nodeDict[node] = self.thelist
                self.callList.append(self.nodeDict)
                self.networklineslist.append(node.lineno)
                

                self.thelist = []
                
                return node

            #Checks whether the function is a return statement and if it contains redirect to add it to network transfer list
            
            elif isinstance(node, ast.Return) and 'redirect' in ast.dump(node):

                for i in range(len(self.thelist)):
                    if self.thelist[i].end_lineno <= node.lineno:
                        self.itemstoremove.append(self.thelist[i])

                self.thelist = list(set(self.thelist) - set(self.itemstoremove))
                self.nodeDict[node] = self.thelist
                self.callList.append(self.nodeDict)
                self.networklineslist.append(node.lineno)
                

                self.thelist = []
                
                return node


            else:
                self.iterChildNodes(node)
        
       
    
    #function that visits all function definitions in the file
    def visit_FunctionDef(self, node):
        testlist = []
        self.generic_visit(node)
        self.iterChildNodes(node)
    
    #Function to remove values from a list

    def remove_values_from_list(the_list, val):
        return [value for value in the_list if value != val]




#Function to parse all network calls. Returns a list of all lines that contain network transfers
def parseNetworkCalls(document, urldoc):
    thefile = open(document, "r")
    lines = thefile.read()
    #Creates the parse tree and uses the Funclister class to iterate the tree
    tree = ast.parse(lines)
    funcLister = FuncLister(urlDoc=urldoc)
    funcLister.checkUrls()
    funcLister.visit(tree)
    print(funcLister.networklineslist)
    print(funcLister.callList)
    return funcLister.networklineslist

#Function to parse all views and returns a list of all lines where the views start
def parseProfiledCode(document, urlpth):
    thefile = open(document, "r")
    lines = thefile.read()
    #Creates the parse tree and uses the Funclister class to iterate the tree
    tree = ast.parse(lines)
    funcLister = FuncLister(urlDoc=urlpth)
    funcLister.checkUrls()
    funcLister.visit(tree)
    print(funcLister.funcList)


string = str(sys.argv[1])
#Cheks the called argument to see whether the file needs to be parsed for network calls or for profiling
if (sys.argv[1] == "Netcalls"):
    string = str(sys.argv[2])
    urlpth = str(sys.argv[3])
    viewspath = string.replace('\\\\', '\\')
    urlsPath = urlpth.replace('\\\\', '\\')
    networkLinesList = parseNetworkCalls(viewspath, urlsPath)
else:
    urlpth = str(sys.argv[3])
    path = str(sys.argv[2])
    fixedPath = path.replace('\\\\', '\\')
    urlsPath = urlpth.replace('\\\\', '\\')
    parseProfiledCode(fixedPath, urlsPath)



