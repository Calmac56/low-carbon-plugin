import * as vscode from 'vscode';
import * as fs from "fs";   


export async function activate(context: vscode.ExtensionContext) {
	

	console.log('Carbon emissions plugin active');

	//Create the status bar item, and get the extension path
	var statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
	var totalcarbon = 0;
	var extensionpath = __dirname;
	var carbonpath = extensionpath?.slice(0, extensionpath.length-3);
	var profilingRun = false;
	var netCallsRun = false;
	var rangelist: vscode.Range[] = [];
	var newRange: vscode.Range[] = [];
		//Used to copy carbon file to compiled folder for testing (Can be removed in release)
	var carbonpathext = carbonpath + "src" + "\\carbon.py";
	var extensionPath = extensionpath + "\\carbon.py";
	fs.copyFile(carbonpathext, extensionPath, (err) => {
		if (err) throw err;
		console.log("Carbon copied");
	});



		//Copies the add profiling script to the required folder
	var profilingPath = carbonpath + "src" + "\\addProfiling.py";
	var extensionPath2 = extensionpath + "\\addProfiling.py";
	fs.copyFile(profilingPath, extensionPath2, (err) => {
	if (err) throw err;
		console.log("Script copied");
	});

	//Show inital status bar

	statusBarItem.text = "Carbon not analysed yet";
	statusBarItem.show();


	//Creates the show emissions command

	let activatecommand = vscode.commands.registerCommand('carbon-emissions-of-networked-applications.showEmissions', async () => 
	{
		//Checks if emissions already been scanned for and if so asks to run hide emissions command before running again
		if(netCallsRun === true){
			vscode.window.showErrorMessage("Already ran profiling, in order to run again use the hide emissions command");
			return;
		}
		
		
		var path = vscode.window.activeTextEditor?.document.uri.fsPath;
		var editor = vscode.window.activeTextEditor;
		


		if (editor === undefined){
			throw vscode.FileSystemError.FileNotFound("Open the views.py file to analyse carbon emissions");
		}

		var urlsFilePath;
		var urlsOption = {
			placeHolder: 'Enter a path',
			title: 'Enter the path to the urls.py file for this django project'
		};

		var q;


		//Creates the input box for asking for the URLs and uses a loop to make sure the data is entered

		await vscode.window.showInputBox(urlsOption).then( async n=> {
			q = n;
			
			
			
		 });

			while(q === undefined || String(q).length === 0 ){
				await vscode.window.showInputBox(urlsOption).then( r=> {
					q = r;

				});

			}

			urlsFilePath = String(q);

		//Get the extension path
		
		var extensionpath =  vscode.extensions.getExtension('carbon-emissions-of-networked-applications')?.extensionPath;
		
		var postArray: string | any[] = [];

		if (extensionpath === undefined){
			extensionpath = __dirname;
		}

		//Sets up options for creating a python shell

		var options = {
			mode: 'text',
			pythonOptions: ['-u'], // get print results in real-time
			scriptPath: extensionpath ,
			args: ["Netcalls", path, urlsFilePath]
		  };

		  //Gets the required module for creating python shells
		  const pythonShell = require('python-shell').PythonShell;

		  /* Uses the pythonshell to run our backend python scripts
		  Creates a prmoise
		  Returns err if script fails
		  Returns success if sucessful and parses the printed results using JSON parse */
		const{success, err='', results} = await new Promise(( resolve, reject) => { pythonShell.run('carbon.py', options, function(err: any,results: any){
			
			if (err) {
				console.log(err);
				reject({success:false, err});
			}

			console.log(results);
			postArray = JSON.parse(results[0]);
			resolve({success: true, results});
		});
			
		});

		

		vscode.window.showInformationMessage('Showing the carbon emissions of the produced code');
	
		if (success){
			console.log(postArray);
		}

		
		
		

		
		let carbonPerGb =  0.01724526701 * 1000 ; //Estimate taken from a 2021 paper in grams CO2
		var carbonPerMb = carbonPerGb/1024;
		var networkCarbon = 0;
		
		let carbonMap = new Map();

		/* Loops through the array of line numbers that need transfer sizes entered
		Highlights the line using revealRange
		Creates an input box asking for the data transfer size and awaits a correct size to be entered and checks its a floawing point number
		 Parses the input as a float and adds it to the map with its line number
		 Increases the total carbon value for network calls*/
		let i = 0;
		while (i< postArray.length){
			let theline = new vscode.Position(postArray[i]-1,0);
			var inputOptions = {
				placeHolder: 'Enter an number',
				title: 'Enter the estimated data size on line ' + (postArray[i]) + ' in megabytes'
			};
			if (editor !== undefined){
				editor.selections = [new vscode.Selection(theline, theline)];
				var range = new vscode.Range(theline, theline);
				editor.revealRange(range);
			}

		var x;
	
		 await vscode.window.showInputBox(inputOptions).then( async z=> {
			x = z;
			
			
			
		 });

			while(x === undefined || isNaN(parseFloat(x)) ){
				await vscode.window.showInputBox(inputOptions).then( y=> {
					 x = y;
				});

			}
		carbonPerMb.toFixed(2);
		var parsedNum = parseFloat(x);
		if (parsedNum !== undefined){
			parsedNum.toFixed(2);
		}
		carbonMap.set(postArray[i], (parsedNum * carbonPerMb).toFixed(2) );
		networkCarbon = networkCarbon + (parsedNum * carbonPerMb);
		 i++;
			
		}

		totalcarbon = networkCarbon;
		if (totalcarbon !== undefined){
			totalcarbon.toFixed(2);
		}
		//Sorts the map into transfer size order
		let sortedCarbon = new Map([...carbonMap.entries()].sort((a, b) => b[1] - a[1]));
		let numItems = sortedCarbon.size;

		//Function that splits the map into even parts and returns size of these even parts in an array
		const splitNumber = (num = 1, parts = 1) => {
			let n = Math.floor(num / parts);
			const arr = [];
			for (let i = 0; i < parts; i++){
			   arr.push(n);
			};
			if(arr.reduce((a, b)=> a + b,0) === num){
			   return arr;
			};
			for(let i = 0; i < parts; i++){
			   arr[i]++;
			   if(arr.reduce((a, b) => a + b, 0) === num){
				  return arr;
			   };
			};
		 };
		 
		let partsArr = splitNumber(numItems, 3);
		if (partsArr === undefined){
			return;
		}

		//Loops through all values in the sorted network calls map
		
		for(let [key, value] of sortedCarbon){
			let theline = new vscode.Position(key-1,0);
			let theline2 = theline;
			if (editor !== undefined){
				theline2 = new vscode.Position(key-1, editor.document.lineAt(key-1).range.end.character);
			}
			
			let linerange = new vscode.Range(theline,theline2);
			//Creates an inital background colour for the line and sets up the hover that states network call emissions
			let decoration = vscode.window.createTextEditorDecorationType({isWholeLine: true, backgroundColor: '#fd0d02'});
			let theHover = new vscode.Hover("This line contributes " + String(value) + " g CO2 carbon emissions from network calls", linerange);

			//Registers the hover so when the line is hovered over it shows the hover
			vscode.languages.registerHoverProvider('python', {
					provideHover(doc, pos, token) : vscode.ProviderResult<vscode.Hover>{
					let range = doc.lineAt(pos);
					let line = doc.lineAt(theline);
					
					

					if (line.lineNumber === range.lineNumber){
						return theHover;
					}
				
				}
		
		});

			/* If statement that sets the colour of each line based on if it is in the green, yellow, or red part */
		
			if(partsArr[0] !== 0){
				decoration = vscode.window.createTextEditorDecorationType({isWholeLine: true, backgroundColor: '#cc3232'});
				partsArr[0] = partsArr[0] - 1;
			}
			else if(partsArr[1] !== 0){
				decoration = vscode.window.createTextEditorDecorationType({isWholeLine: true, backgroundColor: '#e7b416'});
				partsArr[1] = partsArr[1] - 1;
			}
			else{
				decoration = vscode.window.createTextEditorDecorationType({isWholeLine: true, backgroundColor: '#2dc937'});
			}

			
			rangelist.push(linerange);
			newRange.push(linerange);
			editor?.setDecorations(decoration, rangelist);
			rangelist.pop();
			
		}

		totalcarbon = Number(totalcarbon.toFixed(2));
		statusBarItem.text = String(totalcarbon);
		networkCarbon = Number(networkCarbon.toFixed(2));
		//Creates the hovered text in the toolbar in markdown and shows it
		let tooltip = new vscode.MarkdownString().appendMarkdown("## Carbon emissions \n").appendMarkdown("The taskbar number shows the total carbon usage for this application in g of CO2.  \n The amount of CO2 produced from network calls is " + String(networkCarbon));
		statusBarItem.tooltip = tooltip;
		statusBarItem.show();
		netCallsRun = true;
		
		
		

	});

	// This sets the command to hide emissions and deactive the plugin data

	let deactivatecommand = vscode.commands.registerCommand('carbon-emissions-of-networked-applications.hideEmissions', () => 
	{
		var editor = vscode.window.activeTextEditor;
		vscode.window.showInformationMessage('Hiding the carbon emissions of the produced code');
		totalcarbon = 0;
		profilingRun = false;
		netCallsRun = false;
		//Removes all the highlighted lines and sets them back to the editors theme colour
		let thedecoration  = vscode.window.createTextEditorDecorationType({isWholeLine : true, backgroundColor: new vscode.ThemeColor('editor.background')});
		editor?.setDecorations(thedecoration, newRange);

		//Resets the status bar and tooltip text
		statusBarItem.text = "Carbon not analysed yet";
		let tooltip = new vscode.MarkdownString().appendMarkdown("## Carbon emissions \n").appendMarkdown("Carbon emissions not yet analysed");
		statusBarItem.tooltip = tooltip;


		
	} );


	//This sets out the add profiling command which allows the developer to add the profiler to their django project automatically

	let addProfilingCommand = vscode.commands.registerCommand('carbon-emissions-of-networked-applications.addProfiling', async () =>
	{
		var path = vscode.window.activeTextEditor?.document.uri.fsPath;

		//Checks if the settings file is opened to add required settings
		if (path === undefined || !path.includes('settings.py')){
			throw vscode.FileSystemError.FileNotFound("Open the settings.py file to add profiling");
		}
		//Creates text box text
		var inputOptions = {
			placeHolder: 'Enter a path',
			title: 'Enter the path to the urls.py file'
		};

		//Asks developer for path to the main urls file
		var urlPath;
		await vscode.window.showInputBox(inputOptions).then( async result=> {
			urlPath = result;
			
			
			
		});

		var extensionpath = __dirname;
		
		//Creats a vscode terminal to install required pip packagae and displays error if it cant install
		const terminal = vscode.window.createTerminal("django-silk-install");
		terminal.show();
		terminal.sendText("pip install django-silk");
		if (terminal.exitStatus){
			vscode.window.showErrorMessage("Could not install djano silk, Manual install required");
			terminal.dispose();
			return;
		}
		terminal.dispose();

		//Creates a python shell to run remaining install script
		const pythonShell = require('python-shell').PythonShell;
		var options = {
			mode: 'text',
			pythonOptions: ['-u'], // get print results in real-time
			scriptPath: extensionpath ,
			args: [path, urlPath]
		  };

	
		/*Runs the pythonshell with the addprofiling.py script to add required profiler settings 
		Shows an error if uncsuccessful
		Otherwise command finishes*/

		const{success, err='', results} = await new Promise(( resolve, reject) => { pythonShell.run('addProfiling.py', options, function(err: any,results: any){
			
			if (err) {
				console.log(err);
				reject({success:false, err});
				vscode.window.showErrorMessage("Failed to add required settings or url, Was the path to the urls file entered correctly?");
			}

			console.log(results);
			resolve({success: true, results});
		});
			
		});


	});

	//Sets out the command for adding profiling results
	let addProfilingResultsCommand = vscode.commands.registerCommand("carbon-emissions-of-networked-applications.addProfilingResults", async () => 
	{
		let profiledMap = new Map();
		//Checks that show emissions has been run first and that profiling hasnt already been run
		if (netCallsRun ===  false){
			vscode.window.showErrorMessage("Run the show emissions command to add network call emissions first");
			return;
		}
		if(profilingRun === true){
			vscode.window.showErrorMessage("Already added profiling results, in order to re analyse, use the hide emissions command");
			return;
		}
		
		//Checks the views file is open to analyse
		var path = vscode.window.activeTextEditor?.document.uri.fsPath;
		if (path === undefined || !path.includes("views.py")){
			throw vscode.FileSystemError.FileNotFound("Open the views.py file to add profiled carbon emissions");
		}


		var urlsFilePath;
		//Creates the options for the input box
		var urlsOption = {
			placeHolder: 'Enter a path',
			title: 'Enter the path to the urls.py file for this django project'
		};

		var q;

		//Asks the developer to enter the path to the urls.py file

		await vscode.window.showInputBox(urlsOption).then( async n=> {
			q = n;
			
			
			
		 });

			while(q === undefined || String(q).length === 0 ){
				await vscode.window.showInputBox(urlsOption).then( r=> {
					q = r;

				});

			}

			urlsFilePath = String(q);
		var extensionpath = __dirname;


		//Creates the options for the python shell and the shell

		var options = {
			mode: 'text',
			pythonOptions: ['-u'], // get print results in real-time
			scriptPath: extensionpath ,
			args: ["x",path, urlsFilePath]
		  };

		
		const pythonShell = require('python-shell').PythonShell;

		var resultsArray: number[];
		resultsArray = [];
		//Runs the carbon.py script in the shell to find each view to profile
		const{success, err='', results} = await new Promise(( resolve, reject) => { pythonShell.run('carbon.py', options, function(err: any,results: any){
			
			if (err) {
				console.log(err);
				reject({success:false, err});
			}

			console.log(results);
			resultsArray = JSON.parse(results[0]);
			resolve({success: true, results});
		});
			
		});
		var editor = vscode.window.activeTextEditor;
		var totalCpuSeconds = 0.0;
		var profiledCarbon = 0;

		//Creates the input box to get the wattage of the CPU the code was profiled on

		var inputOptions = {
			placeHolder: 'Enter an number',
			title: 'Enter the wattage of the CPU the code was profiled on'
		};
		var cpuWattage;
		await vscode.window.showInputBox(inputOptions).then( async result=> {
			cpuWattage = result;
			
			
			
		 });

		 while(cpuWattage === undefined || isNaN(parseFloat(cpuWattage)) ){
			await vscode.window.showInputBox(inputOptions).then( y=> {
				 cpuWattage = y;
			});

		}

		var intWattage = parseFloat(cpuWattage);

		let i = 0;
		/* 
		Loops through list of views and asks the developer to enter the profile time
		Highlights the line where the view starts */
		while (i< resultsArray.length){
			let theline = new vscode.Position(resultsArray[i]-1,0);
			var inputOptions = {
				placeHolder: 'Enter an number',
				title: 'Enter the cProfile call time for the view starting from line ' + (resultsArray[i]) + ' in seconds'
			};
			if (editor !== undefined){
				editor.selections = [new vscode.Selection(theline, theline)];
				var range = new vscode.Range(theline, theline);
				editor.revealRange(range);
			}

		var x;
	
		 await vscode.window.showInputBox(inputOptions).then( async z=> {
			x = z;
			
			
			
		 });

			while(x === undefined || isNaN(parseFloat(x)) ){
				await vscode.window.showInputBox(inputOptions).then( y=> {
					 x = y;
				});

			}

		
		//Calculates the emissions cost

		totalCpuSeconds = totalCpuSeconds + parseFloat(x);
		
		wattHours = parseFloat(x)/3600 * intWattage;
		kiloWattHours = wattHours/1000;
		profiledCarbon = kiloWattHours * 0.21233 * 1000; //0.21233 is the CO2 per g cost per kwh taken from UK government statistics 2021
		//Adds the emissions cost for each view to a map to display hover on line where the view starts
		profiledMap.set(resultsArray[i], Number(profiledCarbon).toFixed(2));
			
		i++;



		}


		var kiloWattHours;
		var wattHours;

		/* Loops through the map of profiled views and their starting line numbers
		Creates a hover for the emissions cost of the view
		Registers the hover to appear when the line the view starts on is hovered over*/

		for(let [key, value] of profiledMap){
			let theline = new vscode.Position(key-1,0);
			let theline2 = theline;
			if (editor !== undefined){
				theline2 = new vscode.Position(key-1, editor.document.lineAt(key-1).range.end.character);
			}
			
			let linerange = new vscode.Range(theline,theline2);
			let theHover = new vscode.Hover("This view contributes " + String(value) + " g CO2 carbon emissions from cpu profiling", linerange);

			vscode.languages.registerHoverProvider('python', {
					provideHover(doc, pos, token) : vscode.ProviderResult<vscode.Hover>{
					let range = doc.lineAt(pos);
					let line = doc.lineAt(theline);
					
					

					if (line.lineNumber === range.lineNumber){
						return theHover;
					}
				
				}
		
		});
	}


		wattHours = totalCpuSeconds/3600 * intWattage;
		kiloWattHours = wattHours/1000;
		profiledCarbon = kiloWattHours * 0.21233 * 1000;
		totalcarbon = totalcarbon + profiledCarbon;
		totalcarbon = Number(totalcarbon.toFixed(2));
		statusBarItem.text = String(totalcarbon);
		//Adds the profiled emissions to the taskbar so they are displayed with the network transfer emissions
		var tooltipText = new vscode.MarkdownString();
		if(statusBarItem.tooltip !== undefined){
			tooltipText = statusBarItem.tooltip as vscode.MarkdownString;
		}
		profiledCarbon = Number(profiledCarbon.toFixed(2));
		tooltipText.appendMarkdown("\n" ).appendMarkdown("The carbon use from profiled views is " + String(profiledCarbon));
		statusBarItem.tooltip = tooltipText;
		statusBarItem.show();

		profilingRun = true;

		


	

	});


	//Registers all of our plugins commands
	context.subscriptions.push(activatecommand);
	context.subscriptions.push(deactivatecommand);
	context.subscriptions.push(addProfilingCommand);
	context.subscriptions.push(addProfilingResultsCommand);
	return new vscode.DocumentHighlight(new vscode.Range(new vscode.Position(1, 0), new vscode.Position(3, 0)));
	  
	

	

	
}


export function deactivate() {}
