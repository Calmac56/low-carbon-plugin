""" Script that adds the silk profiler to the developers django project """

import sys
#Function that adds required settings for the profile.
#Takes in one document parameter which is the path to the settings.py file
def addProfile(document):
    thefile = open(document, "r")
    lines = thefile.readlines()
    i =0 

    #Checks profile has not alread been added 
    for line in lines:
        if "SILKY_PYTHON_PROFILER" in line:
            print("Profiling already added, skipping")
            return

    #Updates the developers settings.py file with the correct settings
    for i in range(len(lines)):
        if "SECRET_KEY" in lines[i]:
            lines.insert(i+1,"\nSILKY_PYTHON_PROFILER = True\n" )
        
        if "INSTALLED_APPS" in lines[i]:
            print(lines[i])
            lines.insert(i+1, "\t'silk',\n")
        
        if "MIDDLEWARE" in lines[i]:
            lines.insert(i+1, "\t'silk.middleware.SilkyMiddleware',\n")
    
    newfile = open(document, "w")
    newfile.writelines(lines)

#Function that updates the urls.py file with the required url
#Takes in one parameter document which is the path to the urls.py file
def addUrl(document):
    thefile = open(document, 'r+')
    lines = thefile.readlines()
    i = 0
    
    #Skips adding url if it already exists
    for line in lines:
        if "silk" in line:
            print("URL already added, skipping")
            return

    thefile.write("urlpatterns += [path('silk/', include('silk.urls', namespace='silk'))]")


docPath = str(sys.argv[1])
urlPath = str(sys.argv[2])
docPathFixed = docPath.replace('\\\\', '\\')
urlPathFixed = urlPath.replace('\\\\','\\')
addProfile(docPathFixed)
addUrl(urlPathFixed)